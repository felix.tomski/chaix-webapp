FROM python:3.11.4-alpine

WORKDIR /usr/src/demo

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update
RUN apk upgrade
RUN apk add make automake gcc g++ subversion python3-dev gfortran
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . .
