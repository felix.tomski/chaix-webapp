import dash
from dash import Dash, Input, Output, html, callback, dcc
#import dpd_components as dpd
from django_plotly_dash import DjangoDash

import dash_ag_grid as dag
from glob import glob
from os import path
from datetime import datetime
import os

import demo.report_to_csv as r2csv
import demo.dash_chaix_utility as ut

report_path=ut.DAILY_REPORT_DIR

additional_col_confs = {
    'test': {"tooltipField": 'test',
             "tooltipComponentParams": { "color": '#ECEFF4' }},
    'result': {"cellStyle": ut.GRID_CELL_STYLE},
}

column_filters = ('tag', 'runid', 'description')

dates = ut.get_avail_dates(report_path)
latest_date = dates[0]

def summarize_result_by_testname(df):
    aggregate_col = 'result'
    cols = [col for col in r2csv.CSV_HEADER+r2csv.CSV_HEADER_POSTFIX if col not in ('runid', 'jobid', 'testcase', 'result', 'description')]
    return df.groupby(by=cols,
                   as_index=False,
                   dropna=False
                   )[aggregate_col].apply(lambda x: 'success' if x.eq('success').all() else 'failure')

dashboard_name = 'overview-highlevel-daily'
overview_highlevel_daily = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

overview_highlevel_daily.layout = html.Div(
        [
            dcc.Markdown('Overview of daily checks', id='text-info'),
            dcc.Dropdown(dates, None, id='date'),
            html.Div(id='grid-container', children = [ut.get_grid(summarize_result_by_testname(ut.get_df_for_date(latest_date, report_path)), additional_col_confs, column_filters)]),
        ]
    )

@overview_highlevel_daily.callback(
    Output('virtualRowData-grid', 'rowData'),
    Output('text-info', 'children'),
    Input('date', 'value'),
)
def update_graph(date):
    print(f'update_graph {date}')
    if not date:
        date = latest_date
    _df = summarize_result_by_testname(r2csv.report_to_df(ut.get_reports_for_period(date, date, report_path), perf=False))
    res = _df.to_dict(orient='records')
    return (res, f'Overview of daily check for {date}')


