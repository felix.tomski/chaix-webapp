import json
from types import SimpleNamespace
import csv
from copy import deepcopy
from io import StringIO
from pandas import read_csv

CSV_HEADER = ['date',
              'tag',
              'runid',
              'system',
              'partition',
              'environment',
              'test',
              'testcase',
              'result',
              ]
CSV_HEADER_POSTFIX = [
              'tags',
              'jobid',
              'description',
]

CSV_PERF_HEADER = deepcopy(CSV_HEADER)
CSV_PERF_HEADER.extend([
    'perfvar',
    'unit',
    'value',
    'reference',
    'thres_lower',
    'thres_upper',
])

csv_perf_list_header = deepcopy(CSV_HEADER)
csv_perf_list_header.extend([
    'perfvar_x',
    'perfvar_x_unit',
    'perfvar_x_value',
    'perfvar',
    'unit',
    'value'
    'reference',
    'thres_lower',
    'thres_upper',
])

def __getrows(testcase, perf) -> list:
    system, partition = tuple(testcase.system.split(':'))
    row = [
        system,
        partition,
        testcase.environment,
        testcase.name.split(' ')[0],
        testcase.display_name,
        testcase.fail_phase if testcase.fail_phase else 'success',

    ]
    row_post = [
        testcase.tags,
        testcase.jobid,
        testcase.description,
    ]

    # TODO: do not skip list perfvars
    if not perf or not testcase.perfvars:
        return [row + row_post]


    has_list_perfvar = False
    base_perfvar = testcase.perfvars[0]
    if isinstance(base_perfvar.value, list) and len(base_perfvar.value) > 1 and base_perfvar.unit.startswith('x'):
        has_list_perfvar = True
        perfvar_x = testcase.perfvars[0].name
        perfvar_x_unit = testcase.perfvars[0].unit
        perfvar_x_value = testcase.perfvars[0].value

    perfvar_res = []
    for i, perfvar in enumerate(testcase.perfvars):
        if (has_list_perfvar or isinstance(perfvar.value, list) and len(perfvar.value) == 1) and i == 0:
            continue
        l = [] if not has_list_perfvar else [perfvar_x, perfvar_x_unit, perfvar_x_value]

        if isinstance(perfvar.value, list):
            continue

        l += [
            perfvar.name,
            perfvar.unit,
            perfvar.value,
            perfvar.reference,
            perfvar.thres_lower,
            perfvar.thres_upper,
        ]

        perfvar_res.append(row + l + row_post)
    return perfvar_res


def __get_csv_writer(output_fp, perf):
    _header = deepcopy(CSV_HEADER) if not perf else deepcopy(CSV_PERF_HEADER)
    _header += CSV_HEADER_POSTFIX
    csv_writer = csv.writer(output_fp, delimiter='|', quoting=csv.QUOTE_NONNUMERIC)
    csv_writer.writerow(_header)
    return csv_writer

def __report2csv(report, csv_writer, perf, tag):
    date = report.session_info.time_start.split('T')[0]
    for run in report.runs:
        for testcase in run.testcases:
            if perf and not testcase.perfvars:
                continue
            for r in __getrows(testcase, perf):
                csv_writer.writerow([
                    date,
                    tag,
                    run.runid,
                ] + r)


def __aggregate_reruns(df):
    cols = [col for col in CSV_PERF_HEADER+CSV_HEADER_POSTFIX if col not in ('value', 'runid', 'jobid', 'result')]
    return df.groupby(by=cols,
                      as_index=False,
                      dropna=False
                      ).agg({
        'result': lambda x: 'success' if x.eq('success').all() else 'failure',
        'value': 'mean'
    })

def report_to_csv(reports, output_fp, perf=False, tag=None):
    csv_writer = __get_csv_writer(output_fp, perf)
    for report_file in reports:
        with open(report_file, 'r') as fp:
            report = json.load(fp, object_hook=lambda d: SimpleNamespace(**d))
        __report2csv(report, csv_writer, perf, tag)
    output_fp.seek(0)

def report_str_to_csv(reports, output_fp, perf=False, tag=None):
    csv_writer = __get_csv_writer(output_fp, perf)
    for report_content in reports:
        report = json.loads(report_content, object_hook=lambda d: SimpleNamespace(**d))
        __report2csv(report, csv_writer, perf, tag)
    output_fp.seek(0)

def report_to_df(reports, perf=False, tag=None, aggregate_reruns=True):
    csv_fp = StringIO()
    report_to_csv(reports, csv_fp, perf, tag)
    _df = read_csv(csv_fp, delimiter='|', parse_dates=['date'])
    csv_fp.close()
    if aggregate_reruns and perf:
        _df = __aggregate_reruns(_df)

    return _df

def report_str_to_df(reports, perf=False, tag=None, aggregate_reruns=True):
    csv_fp = StringIO()
    report_str_to_csv(reports, csv_fp, perf, tag)
    _df = read_csv(csv_fp, delimiter='|', parse_dates=['date'])
    csv_fp.close()
    if aggregate_reruns and perf:
        _df = __aggregate_reruns(_df)

    return _df
