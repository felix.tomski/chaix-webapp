import dash
from dash import Dash, Input, State, Output, html, callback, dcc
import plotly.graph_objs as go
import plotly.express as px
#import dpd_components as dpd
import numpy as np
from django_plotly_dash import DjangoDash

import dash_ag_grid as dag
import os

import demo.report_to_csv as r2csv

from glob import glob
import demo.dash_chaix_utility as ut


defaultColDef = {
    "flex": 1,
    "minWidth": 150,
    "sortable": True,
    "resizable": True,
    "filter": True,
}

report_path=ut.DAILY_REPORT_DIR
init_report = os.path.join(report_path, os.listdir(report_path)[0])
df = r2csv.report_to_df(glob(f'{init_report}/**/*json', recursive=True), perf=True)

cellStyle = {
    "styleConditions": [
        {
            "condition": "params.data.result == 'success'",
            "style": {"backgroundColor": "#A3BE8C"},
        },
        {
            "condition": "params.data.result == 'performance'",
            "style": {"backgroundColor": "#D08770", "color": "white"},
        },
        {
            "condition": "params.data.result != 'success'",
            "style": {"backgroundColor": "#BF616A", "color": "white"},
        },
    ]
}

additional_col_confs = {
    'test': {"tooltipField": 'test',
             "tooltipComponentParams": { "color": '#ECEFF4' },
             },
    'testcase': { "checkboxSelection": True, "headerCheckboxSelection": True },
    'result': {"cellStyle": cellStyle},
}

colum_filters = ('description')


def get_barplot(_df, testcase_name, perfvar):
    if not testcase_name:
        return px.bar(_df, x="perfvar", y="value", color="environment", pattern_shape='system', barmode="group")
    dff = df[df['testcase'] == testcase_name]
    if not perfvar:
        return px.bar(dff, x="perfvar", y="value", color="environment", pattern_shape='system', barmode="group",
                      labels={'value': dff.unit.iloc[0]}, title=testcase_name)

    dff = dff[dff['perfvar'] == perfvar]

    return px.bar(dff, x="perfvar", y="value", color="environment", pattern_shape='system', barmode="group",
                  labels={'value': dff.unit.iloc[0]}, title=testcase_name)

def get_testcase_div(_df, idx):
    return html.Div(id={"index": idx, "name": "container"}, children=[
        dcc.Markdown(_df.testcase.unique()[idx], id={"index": idx, 'name': 'description'}),
        dcc.Graph(id={"index": idx, 'name': 'graph'}, style={'display': 'block'},
                  figure=get_barplot(testcase_name=_df.testcase.unique()[idx], perfvar=''))
    ])

def get_grid(_df):
    columns = [{'field': i, **additional_col_confs.get(i, {})} for i in _df.columns if i not in colum_filters]
    return dag.AgGrid(
        id="selection-checkbox-grid",
        columnDefs=columns,
        rowData=_df.to_dict("records"),
        defaultColDef=defaultColDef,
        dashGridOptions={"rowSelection":"multiple"},
    )


dashboard_name = 'dash_chaix_perf_overview'
dash_chaix_perf_overview = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

dash_chaix_perf_overview.layout = html.Div([
    dcc.Upload(
        id='upload-data-base',
        multiple=True,
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
    ),
    html.Div(id='output-data-upload'),
    html.Div(id="selections-checkbox-output"),
    html.Div(id='graph-container')
],
                                           style={"margin": 20},
                                           )

#@dash_chaix_perf_overview.callback(
#    Output('output-data-upload', 'children'),
#    Output('graph-container', 'children'),
#    Input('upload-data-base', 'contents'),
#    State('upload-data-base', 'filename'),
#    State('upload-data-base', 'last_modified')
#)
#def update_graphs(contents, filename, last_modified):
#    _df = parse_contents(list_of_contents, list_of_names, list_of_dates, 'old-kernel')
#    return ([get_testcase_div(idx) for idx in range(len(df.testcase.unique()))]
#    )
