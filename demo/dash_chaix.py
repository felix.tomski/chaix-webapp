import dash
from dash import Dash, Input, Output, html, callback, dcc
#import dpd_components as dpd
from django_plotly_dash import DjangoDash

import dash_ag_grid as dag
from glob import glob

import demo.report_to_csv as r2csv
import demo.dash_chaix_utility as ut

report_path=ut.DAILY_REPORT_DIR

additional_col_confs = {
    'test': {"tooltipField": 'test',
             "tooltipComponentParams": { "color": '#ECEFF4' }},
    'result': {"cellStyle": ut.GRID_CELL_STYLE},
}

column_filters = ('tag', 'runid', 'description')

df = ut.get_df_for_date(ut.get_avail_dates(report_path)[0], report_path)


dashboard_name = 'dash_chaix_overview'
dash_chaix_overview = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

dash_chaix_overview.layout = html.Div(
        [
            dcc.Markdown("Overview of daily checks"),
            dcc.DatePickerRange(
                id='select-date',
                start_date_placeholder_text="Start Period",
                end_date_placeholder_text="End Period",
                calendar_orientation='vertical',
            ),
            html.Div(id='grid-container', children = [ut.get_grid(df, additional_col_confs, column_filters)]),
        ]
    )



@dash_chaix_overview.callback(
    Output('virtualRowData-grid', 'rowData'),
    Input('select-date', 'start_date'),
    Input('select-date', 'end_date'),
)
def update_graph(start_date, end_date):
    if start_date and end_date:
        return r2csv.report_to_df(ut.get_reports_for_period(start_date, end_date, f'{report_path}'), perf=False).to_dict(orient='records')
    
    return df.to_dict(orient='records')
