import dash
from dash import Dash, Input, Output, html, callback, dcc
#import dpd_components as dpd
from django_plotly_dash import DjangoDash

import dash_ag_grid as dag
from glob import glob

import demo.report_to_csv as r2csv
import demo.dash_chaix_utility as ut


report_path=ut.DAILY_REPORT_DIR
df = r2csv.report_to_df(glob(f'{report_path}/**/*json', recursive=True), perf=True)

def get_graphs(_df, idx, testcase, start_date, end_date):
    return [dcc.Graph(id={"index": idx, 'name': 'graph'}, style={'display': 'block'},
                               figure=ut.get_timeline_plot(_df, testcase_name=testcase, perfvar=perfvar, start_date=start_date, end_date=end_date))
                    for perfvar in _df.perfvar.unique()]

def get_testcase_div(_df, idx, start_date=None, end_date=None):
    testcase = _df.testcase.unique()[idx]
    _df = _df[_df['testcase'] == testcase]
    return html.Div(id={"index": idx, "name": "container"},
                    children=[dcc.Markdown(f'### {testcase}\n{_df.description.iloc[0]}', id={"index": idx, 'name': 'description'})] +
                    get_graphs(_df, idx, testcase, start_date, end_date)
                    )

dashboard_name = 'dash_chaix_perf_timeline_overview'
dash_chaix_perf_timeline_overview = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

dash_chaix_perf_timeline_overview.layout = html.Div(children=[
    html.H1(
        children='Performance over time',
        style={
            'textAlign': 'center',
        }
    ),
    dcc.DatePickerRange(
        id='select-date',
        start_date_placeholder_text="Start Period",
        end_date_placeholder_text="End Period",
        calendar_orientation='vertical',
    ),
    html.Div(id='graph-container', children = [get_testcase_div(df, idx) for idx in range(len(df.testcase.unique()))])
])


@dash_chaix_perf_timeline_overview.callback(
    Output('graph-container', 'children'),
    Input('select-date', 'start_date'),
    Input('select-date', 'end_date'),
)
def update_graph(start_date, end_date):
    _df = ut.get_time_filtered_df(df, start_date, end_date)
    return [get_testcase_div(_df, idx, start_date=start_date, end_date=end_date) for idx in range(len(_df.testcase.unique()))]

