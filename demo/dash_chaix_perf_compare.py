import dash
from dash import Dash, Input, Output, html, callback, dcc, State, dash_table
import plotly.graph_objs as go
import plotly.express as px
#import dpd_components as dpd
from django_plotly_dash import DjangoDash

import dash_ag_grid as dag
import pandas as pd
import base64

import string
import random
import pandas as pd

import demo.report_to_csv as r2csv

import os
from tempfile import TemporaryDirectory

#import plotly.io as pio   
#pio.kaleido.scope.mathjax = None

defaultColDef = {
    "flex": 1,
    "minWidth": 150,
    "sortable": True,
    "resizable": True,
    "filter": True,
}

cellStyle = {
    "styleConditions": [
        {
            "condition": "params.data.result == 'success'",
            "style": {"backgroundColor": "#A3BE8C"},
        },
        {
            "condition": "params.data.result == 'performance'",
            "style": {"backgroundColor": "#D08770", "color": "white"},
        },
        {
            "condition": "params.data.result != 'success'",
            "style": {"backgroundColor": "#BF616A", "color": "white"},
        },
    ]
}

additional_col_confs = {
    'test': {"tooltipField": 'test',
             "tooltipComponentParams": { "color": '#ECEFF4' },
             },
    'testcase': { "checkboxSelection": True, "headerCheckboxSelection": True },
    'result': {"cellStyle": cellStyle},
}

colum_filters = ('description')


def get_barplot(_df, testcase_name, perfvar):
    if not testcase_name:
        return px.bar(_df, x="perfvar", y="value", color="environment", pattern_shape='system', barmode="group")
    dff = _df[_df['testcase'] == testcase_name]
    if not perfvar:
        perfvar = dff.perfvar.iloc[0]

    dff = dff[dff['perfvar'] == perfvar]
    fig = px.bar(dff, x="perfvar", y="value", color="environment", pattern_shape='tag', barmode="group",
                 labels={'value': ("" if dff.unit.isnull().values.any() else dff.unit.iloc[0])},
                 width=1000,
                 title=testcase_name
                 )

    return fig


def get_graphs(_df, idx, testcase):
    return [dcc.Graph(id={"index": idx, 'name': 'graph'}, style={'display': 'block'},
                      figure=get_barplot(_df, testcase_name=testcase, perfvar=perfvar))
        for perfvar in _df.perfvar.unique()]

def get_testcase_div(_df, idx):
    testcase = _df.testcase.unique()[idx]
    _df = _df[_df['testcase'] == testcase]
    return html.Div(id={"index": idx, "name": "container"},
                    children=[dcc.Markdown(f'### {testcase}\n{_df.description.iloc[0]}', id={"index": idx, 'name': 'description'})] +
                    get_graphs(_df, idx, testcase)
                    )

def write_report_to_pdf(children, file):
    with TemporaryDirectory() as tmpdir:
        for graph_children in children:
            for graph in graph_children['props']['children']:
                if not 'props' in graph:
                    continue
                if not 'figure' in graph['props']:
                    continue
                hash = ''.join(random.choices(string.ascii_uppercase + string.digits, k=15))
                pio.write_image(graph['props']['figure'], f'{tmpdir}/{hash}.pdf')
        os.system(f'pdfunite {tmpdir}/*.pdf {file}')

def get_grid(_df):
    columns = [{'field': i, **additional_col_confs.get(i, {})} for i in _df.columns if i not in colum_filters]
    return dag.AgGrid(
        id="selection-checkbox-grid",
        columnDefs=columns,
        rowData=_df.to_dict("records"),
        defaultColDef=defaultColDef,
        dashGridOptions={"rowSelection":"multiple", "domLayout":"autoHeight"},
        csvExportParams={ "fileName": "ag_grid_test.csv", },

    )

def get_diff_grid(df1, df2):
    #    cols = [col for col in r2csv.CSV_PERF_HEADER+r2csv.CSV_HEADER_POSTFIX if col not in ('value', 'result', 'date', 'runid', 'jobid')]
    to_drop = ['date', 'tag', 'tags', 'description', 'result']
    _df = pd.merge(df1.drop(columns=to_drop), df2.drop(columns=to_drop), on=['system', 'partition', 'environment', 'test', 'testcase', 'perfvar', 'unit', 'reference', 'thres_lower', 'thres_upper'])
    _df['value_diff'] = _df['value_y'] - _df['value_x']
    _df['rel_diff'] = (_df['value_diff'] / _df['value_x'] * 100)
    rearrange_cols = ['system', 'partition', 'environment', 'test', 'testcase', 'perfvar', 'unit', 'value_x', 'value_y', 'rel_diff', 'value_diff']
    return _df[rearrange_cols + list(set(_df.columns.tolist()) - set(rearrange_cols))]
    

def parse_contents(contents, filenames, dates, tag='user'):
    content_strs = []
    for content, filename in zip(contents, filenames):
        if 'json' in filename:
            _, content_string = content.split(',')
            content_strs.append(base64.b64decode(content_string))

    return r2csv.report_str_to_df(content_strs, perf=True, tag=tag)

dashboard_name = 'dash_chaix_perf_compare'
dash_chaix_perf_compare = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

dash_chaix_perf_compare.layout = html.Div([
    dcc.Markdown("Upload a performance report for comparison"),
    dcc.Upload(
        id='upload-data-base',
        multiple=True,
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
    ),
    dcc.Upload(
        id='upload-data-new',
        multiple=True,
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
    ),
    html.Div(dcc.Input(id='pdf-name-input', type='text')),
    html.Button('Save as PDF', id='save-as-pdf', n_clicks=0),
    html.Div(id='save-feedback',
             children='Enter a value and press submit'),
    dcc.Checklist(["Plot"], [], id="plot-checklist", inline=True),
    html.Button("Download CSV", id="csv-button", n_clicks=0),
    html.Div(id='output-data-upload'),
    html.Div(id='graph-container')
])

@dash_chaix_perf_compare.callback(
    Output("selection-checkbox-grid", "exportDataAsCsv"),
    Input("csv-button", "n_clicks"),
)
def export_data_as_csv(n_clicks):
    print('Downloading')
    if n_clicks:
        return True
    return False


@dash_chaix_perf_compare.callback(
    Output('output-data-upload', 'children'),
    Output('graph-container', 'children'),
    Input('upload-data-base', 'contents'),
    Input('upload-data-new', 'contents'),
    Input('plot-checklist', 'value'),
    State('upload-data-base', 'filename'),
    State('upload-data-base', 'last_modified'),
    State('upload-data-new', 'filename'),
    State('upload-data-new', 'last_modified')
)
def update_output(list_of_contents, new_contents, enable_plots, list_of_names,
                  list_of_dates, new_names, new_dates):
    if list_of_contents is None and new_contents is None:
        return ([], [])

    if list_of_contents and new_contents is None:
        _df = parse_contents(list_of_contents, list_of_names, list_of_dates)
        print(list(_df))
        return (get_grid(_df),
                [get_testcase_div(_df, idx) for idx in range(len(_df.testcase.unique()))] if enable_plots else []
                )

    elif list_of_contents and new_contents:
        _df1 = parse_contents(list_of_contents, list_of_names, list_of_dates, 'old-kernel')
        _df2 = parse_contents(new_contents, new_names, new_dates, 'new-kernel')
        _df = pd.concat([_df1, _df2])
        return (get_grid(get_diff_grid(_df1, _df2)),
                [get_testcase_div(_df, idx) for idx in range(len(_df.testcase.unique()))] if enable_plots else []
                )


@dash_chaix_perf_compare.callback(
    Output('save-feedback', 'children'),
    Input('save-as-pdf', 'n_clicks'),
    Input('graph-container', 'children'),
    State('pdf-name-input', 'value'),
)
def save_as_pdf(n_clicks, children, file_name):
    if not file_name:
        file_name = '/tmp/performance_comparison.pdf'

    if not children:
        return

    write_report_to_pdf(children, file_name)
    return f'Report saved to {file_name}'
