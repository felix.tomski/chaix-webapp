import dash
from dash import Dash, Input, Output, html, callback, dcc
#import dpd_components as dpd
from django_plotly_dash import DjangoDash
from dash import dash_table


import dash_ag_grid as dag
from glob import glob
from os import path
import pandas as pd

import demo.report_to_csv as r2csv
import demo.dash_chaix_utility as ut

report_path=ut.DAILY_REPORT_DIR

df = pd.read_csv('/home/flx/cluster_testsuite/server/data/av_modules2.csv', sep=',')

#mask = df.applymap(type) != bool
#d = {True: 'TRUE', False: 'FALSE'}
#df = df.where(mask, df.replace(d))
#print(df)

dashboard_name = 'available-modules'
dash_available_modules = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

#dash_available_modules.layout = html.Div(
#    [
#        html.Div(id='grid-container', children = [
#            dash_table.DataTable(
#                df.to_dict('records'),
#                [{"name": i, "id": i} for i in df.columns]
#            )
#        ]),
#    ]
#)

dash_available_modules.layout = html.Div(
    [
        dag.AgGrid(
            id='av-mod-grid',
            rowData=df.to_dict('records'),
            columnDefs=list(df.columns),
#            columnSize="autoSize",
#            dashGridOptions={"domLayout": "autoHeight"},
#            defaultColDef={"filter": True, "sortable": True, 'resizable': True},
        )
    ]
)

#@overview_highlevel_daily.callback(
#    Output('virtualRowData-grid', 'rowData'),
#    Output('text-info', 'children'),
#    Input('date', 'value'),
#)
#def update_graph(date):
#    print(f'update_graph {date}')
#    if not date:
#        date = latest_date
#    _df = summarize_result_by_testname(r2csv.report_to_df(ut.get_reports_for_period(date, date, report_path), perf=False))
#    res = _df.to_dict(orient='records')
#    return (res, f'Overview of daily check for {date}')



