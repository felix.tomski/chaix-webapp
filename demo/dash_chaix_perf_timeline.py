import dash
from dash import Dash, Input, Output, html, callback, dcc
import plotly.express as px
#import dpd_components as dpd
from django_plotly_dash import DjangoDash

import dash_ag_grid as dag
from glob import glob

import demo.report_to_csv as r2csv
import demo.dash_chaix_utility as ut


report_path=ut.DAILY_REPORT_DIR
df = r2csv.report_to_df(glob(f'{report_path}/**/*json', recursive=True), perf=True)

def get_perfvars(testcase):
    return df[df['testcase'] == testcase]['perfvar']

def get_description(testcase):
    return df[df['testcase'] == testcase].description.iloc[0]

dashboard_name = 'dash_chaix_perf_timeline'
dash_chaix_perf_timeline = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

dash_chaix_perf_timeline.layout = html.Div(children=[
    html.H1(
        children='Performance over time',
        style={
            'textAlign': 'center',
        }
    ),
    html.Div([
        dcc.Dropdown(
            df['testcase'].unique(),
            df.testcase.iloc[0],
            id='crossfilter-testcase',
        ),
    ],
             style={'width': '100%', 'display': 'inline-block'}),
    html.Div([
        dcc.Dropdown(
            options=get_perfvars(df.testcase.iloc[0]).unique(),
            value=get_perfvars(df.testcase.iloc[0]).iloc[0],
            id='crossfilter-perfvar',
        ),
    ],
             style={'width': '100%', 'display': 'inline-block'}),
    dcc.DatePickerRange(
        id='select-date',
        start_date_placeholder_text="Start Period",
        end_date_placeholder_text="End Period",
        calendar_orientation='vertical',
    ),
    html.Div(id='crossfilter-description', children=get_description(df.testcase.iloc[0]), style={
        'textAlign': 'center',
    }),

    dcc.Graph(
        id='example-graph-2',
        figure=ut.get_timeline_plot(df, df.testcase.iloc[0], get_perfvars(df.testcase.iloc[0]).iloc[0],
                                 df.date.iloc[-1], df.date.iloc[0])
    )
])


@dash_chaix_perf_timeline.callback(
    Output('example-graph-2', 'figure'),
    Output('crossfilter-perfvar', 'options'),
    Output('crossfilter-perfvar', 'value'),
    Output('crossfilter-description', 'children'),
    Input('crossfilter-testcase', 'value'),
    Input('crossfilter-perfvar', 'value'),
    Input('select-date', 'start_date'),
    Input('select-date', 'end_date'),
)
def update_graph(testcase_name, perfvar, start_date, end_date):
    perfopts = get_perfvars(testcase_name)
    if not perfvar or perfvar not in perfopts.unique():
        perfvar = perfopts.iloc[0]
    return (ut.get_timeline_plot(df, testcase_name=testcase_name, perfvar=perfvar,
                              start_date=start_date, end_date=end_date),
            perfopts.unique(),
            perfvar,
            get_description(testcase_name)
            )
