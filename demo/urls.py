# pylint: disable=wrong-import-position,wrong-import-order

from django.contrib import admin
from django.urls import include, path

from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static

# Load demo plotly apps - this triggers their registration
import demo.dash_chaix        # pylint: disable=unused-import
import demo.dash_chaix_performance        # pylint: disable=unused-import
import demo.dash_chaix_perf_overview        # pylint: disable=unused-import
import demo.dash_chaix_perf_timeline        # pylint: disable=unused-import
import demo.dash_chaix_perf_timeline_overview        # pylint: disable=unused-import
import demo.dash_chaix_perf_compare        # pylint: disable=unused-import
import demo.overview_single_date        # pylint: disable=unused-import
import demo.overview_highlevel_daily    # pylint: disable=unused-import
import demo.dash_available_modules    # pylint: disable=unused-import


from django_plotly_dash.views import add_to_session

from .views import dash_chaix_overview_view, dash_chaix_perf_single, dash_chaix_perf_overview, dash_chaix_perf_timeline, dash_chaix_perf_timeline_overview, dash_chaix_perf_compare, OverviewDate, OverviewHighlevelDate, dash_available_modules


urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name="home"),
    path('overview', dash_chaix_overview_view, name="overview"),
    path('perf-single', dash_chaix_perf_single, name="perf-single"),
    path('perf-overview', dash_chaix_perf_overview, name="perf-overview"),
    path('perf-timeline', dash_chaix_perf_timeline, name="perf-timeline"),
    path('perf-timeline-overview', dash_chaix_perf_timeline_overview, name="perf-timeline-overview"),
    path('perf-compare', dash_chaix_perf_compare, name="perf-compare"),
    path('available-modules', dash_available_modules, name="available-modules"),
    path('daily/<str:date>', OverviewDate, name="overview-daily"),
    path('daily', OverviewDate, name="overview-daily"),
    path('hl-daily/<str:date>', OverviewHighlevelDate, name="overview-highlevel-daily"),
    path('hl-daily', OverviewHighlevelDate, name="overview-highlevel-daily"),
    path('admin/', admin.site.urls),
    path('django_plotly_dash/', include('django_plotly_dash.urls')),
    path('demo-session-var', add_to_session, name="session-variable-example"),
]


# Add in static routes so daphne can serve files; these should
# be masked eg with nginx for production use

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
