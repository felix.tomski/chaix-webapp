import dash
from dash import Dash, Input, Output, html, callback, dcc, ctx
import plotly.graph_objs as go
import plotly.express as px
#import dpd_components as dpd
from django_plotly_dash import DjangoDash

import os
from glob import glob

import demo.dash_chaix_utility as ut

report_path = ut.DAILY_REPORT_DIR
dates = ut.get_avail_dates(report_path)
df = ut.get_df_for_date(dates[0], report_path, perf=True)

def get_barplot(_df, testcase_name, perfvar):
    if not testcase_name:
        return px.bar(_df, x="perfvar", y="value", color="environment", pattern_shape='system', barmode="group")
    dff = _df[_df['testcase'] == testcase_name]
    if not perfvar:
        perfvar = dff.unit.iloc[0]

    dff = dff[dff['perfvar'] == perfvar]
    x_val = 'perfvar_x_value' if 'perfvar_x' in dff else 'perfvar'
    res = px.bar(dff, x=x_val, y="value", orientation='v', color="environment", pattern_shape='system', barmode="group",
                      labels={'value': perfvar})
    if dff.reference.iloc[0]:
        res.add_hline(y=dff.reference.iloc[0],
                      fillcolor='black',
                      annotation_text=f'Ref: {dff.reference.iloc[0]}',
                      annotation_position='bottom right')
    return res


def get_perfvars(_df, testcase):
    return _df[_df['testcase'] == testcase]['perfvar']

def get_description(_df, testcase):
    return _df[_df['testcase'] == testcase].description.iloc[0]

dashboard_name = 'dash_chaix_perf_single'
dash_chaix_perf_single = DjangoDash(name=dashboard_name,
                           serve_locally=True,
                           # app_name=app_name
                          )

dash_chaix_perf_single.layout = html.Div(children=[
    html.H1(
        children='Performance report',
        style={
            'textAlign': 'center',
        }
    ),
    html.Div([
        dcc.Dropdown(
            dates,
            dates[0],
            id='crossfilter-report-date',
        ),
    ],
             style={'width': '100%', 'display': 'inline-block'}),
    html.Div([
        dcc.Dropdown(
            df['testcase'].unique(),
            df.testcase.iloc[0],
            id='crossfilter-testcase',
        ),
    ],
             style={'width': '100%', 'display': 'inline-block'}),
    html.Div([
        dcc.Dropdown(
            options=get_perfvars(df, df.testcase.iloc[0]).unique(),
            value=get_perfvars(df, df.testcase.iloc[0]).iloc[0],
            id='crossfilter-perfvar',
        ),
    ],
             style={'width': '100%', 'display': 'inline-block'}),
    html.Div(id='crossfilter-description', children=get_description(df, df.testcase.iloc[0]), style={
        'textAlign': 'center',
    }),

    dcc.Graph(
        id='example-graph-2',
        figure=get_barplot(df, df.testcase.iloc[0], get_perfvars(df, df.testcase.iloc[0]).iloc[0])
    )
])

@dash_chaix_perf_single.callback(
    Output('example-graph-2', 'figure'),
    Output('crossfilter-perfvar', 'options'),
    Output('crossfilter-perfvar', 'value'),
    Output('crossfilter-description', 'children'),
    Output('crossfilter-testcase', 'options'),
    Input('crossfilter-testcase', 'value'),
    Input('crossfilter-report-date', 'value'),
    Input('crossfilter-perfvar', 'value'),
)
def update_graph(testcase_name, date, perfvar):
    #new_df = get_df(os.path.join(report_path, date))
    new_df = df
    perfopts = get_perfvars(new_df, testcase_name)
    if not perfvar or perfvar not in perfopts.values:
        perfvar = perfopts.iloc[0]
    return (get_barplot(new_df, testcase_name=testcase_name, perfvar=perfvar),
            perfopts.unique(),
            perfvar,
            get_description(new_df, testcase_name),
            new_df.testcase.unique()
            )

