import os
from datetime import datetime
from glob import glob
import demo.report_to_csv as r2csv
import dash_ag_grid as dag
import plotly.express as px

REPORTS_BASE_DIR = os.environ.get('CHAIX_REPORT_BASE_DIR', os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data'))
DAILY_REPORT_DIR = os.path.join(REPORTS_BASE_DIR, 'ci_reports/daily')

GRID_CELL_STYLE = {
    "styleConditions": [
        {
            "condition": "params.data.result == 'success'",
            "style": {"backgroundColor": "#A3BE8C"},
        },
        {
            "condition": "params.data.result == 'performance'",
            "style": {"backgroundColor": "#D08770", "color": "white"},
        },
        {
            "condition": "params.data.result != 'success'",
            "style": {"backgroundColor": "#BF616A", "color": "white"},
        },
    ]
}


def get_avail_dates(basepath, start_date=None, end_date=None):
    format = '%Y-%m-%d'
    date_dirs = sorted([datetime.strptime(os.path.basename(os.path.normpath(dir)), format) for dir in glob(f'{basepath}/*/')], reverse=True)
    date_dirs = [d.strftime(format) for d in date_dirs
        if (not start_date or d >= datetime.strptime(start_date, format)) and
        (not end_date or d <= datetime.strptime(end_date, format))]
    return date_dirs

    
def get_reports_for_period(start_date, end_date, basepath):
    res = []
    for date in get_avail_dates(basepath, start_date, end_date):
        res += glob(f'{basepath}/{date}/**/*.json', recursive=True)
    return res


def get_df_for_date(date, path, perf=False):
    return r2csv.report_to_df(glob(f'{path}/{date}/**/*.json', recursive=True), perf=perf)


def get_grid(_df, additional_col_confs={}, column_filters=()):
   columns = [{'field': i, **additional_col_confs.get(i, {})} for i in _df.columns if i not in column_filters]
   return dag.AgGrid(
                id="virtualRowData-grid",
                columnSize="autoSize",
                dashGridOptions={"domLayout": "autoHeight", "tooltipShowDelay": 300, 'alwaysMultiSort': True},
                rowData=_df.to_dict('records'),
                columnDefs=columns,
                defaultColDef={"filter": True, "sortable": True, 'resizable': True, "tooltipComponent": "CustomTooltip"},
#                rowModelType="infinite",
            )


def get_time_filtered_df(_df, start_date, end_date):
    if start_date is None and end_date is None: 
        return _df
    try:
        start_date = start_date.strftime('%Y%m%d') if start_date else None
        end_date = end_date.strftime('%Y%m%d') if end_date else None
    except Exception:
        start_date = start_date.replace('-', '') if start_date else None
        end_date = end_date.replace('-', '') if end_date else None

    before = f'{start_date} <= ' if start_date else ''
    after = f' <= {end_date}' if end_date else ''
    res = _df.query(f'{before}date{after}')

    return res

def get_timeline_plot(_df, testcase_name, perfvar, start_date, end_date):
    if not testcase_name:
        return None
    dff = _df[_df['testcase'] == testcase_name]
    if perfvar:
        dff = dff[dff['perfvar'] == perfvar]
    dff = get_time_filtered_df(dff, start_date, end_date)
    if dff.empty:
        return None

    res = px.scatter(dff, x="date", y="value", color="environment", symbol='system',
                      labels={'value': dff.unit.iloc[0]})
    res.update_xaxes(
        dtick="D1",
        tickformat="%e %b\n%Y")
    if dff.reference.iloc[0]:
        res.add_hline(y=dff.reference.iloc[0],
                      fillcolor='black',
                      annotation_text=f'Ref: {dff.reference.iloc[0]}',
                      annotation_position='bottom right')

    return res
