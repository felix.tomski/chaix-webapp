# Webapp for visualization of ReFrame results

## Start demo
Setup python environment:
```
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

Start server `./prepare_demo` to start server at 127.0.0.1:8000.
